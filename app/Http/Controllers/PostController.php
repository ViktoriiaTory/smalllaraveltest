<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Show the application postsline.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with('user')->paginate(50);

        return view('posts.index', ['posts' => $posts]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(int $id)
    {
        $post = Post::findOrFail($id);

        return view('posts.show', ['post' => $post]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * @param PostRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PostRequest $request) /*validation example through custom validation rule class */
    {
        Storage::putFileAs('public/uploads', $request->file('photo'), $request->file('photo')
            ->getClientOriginalName());

        $post = Post::create(['title'   => $request->title, 'body' => $request->body,
                              'user_id' => auth()->user()->id, 'photo' => $request->file('photo')
                ->getClientOriginalName()]);
        if ($post) {
            return redirect()->route('posts')->with('message', 'Post was added successfully !');
        } else {
            return redirect()->route('posts')->with('message', 'Something went wrong!');
        }
    }

    /**
     * Edit post
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id, Request $request)
    {
        $post = Post::findOrFail($id);
        if ($request->isMethod('post')) {
            $this->validate($request, (new PostRequest)->rules());
            Storage::putFileAs('public/uploads', $request->file('photo'), $request->file('photo')
                ->getClientOriginalName());
            if ($post->save($request)) {
                return redirect()->route('posts')->with('message', 'Post was successfully updated!');
            }
        }
        return view('posts.edit', ['post' => $post]);
    }

    /**
     * Delete post
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $post = Post::findOrFail($id);
        if ($post->delete()) {
            return redirect()->route('posts')->with('message', 'Post was successfully deleted!');
        } else {
            return redirect()->route('posts')->with('message', 'Something went wrong!');
        }
    }
}
