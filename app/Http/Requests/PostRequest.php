<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'   => 'unique:posts,title,title|min:3',
            'user_id' => 'exists:posts,user_id|integer|min:1',
            'body'    => 'required|min:10',
            'photo'   => 'required'
        ];
    }
}
