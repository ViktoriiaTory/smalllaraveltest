<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    /*POSTS ROUTES GROUP*/
    Route::prefix('posts')->group(function () {
        Route::get('/', 'PostController@index')->name('posts');
        Route::get('/create', 'PostController@create')->name('posts.create');
        Route::post('/store', 'PostController@store')->name('posts.store');
        Route::get('/{id}', 'PostController@show')->where('id', '[0-9]+')->name('posts.show');
        Route::match(['get', 'post'], '/{id}/edit', 'PostController@edit')->where('id', '[0-9]+')->name('posts.edit');
        Route::get('/{id}/delete', 'PostController@destroy')->where('id', '[0-9]+')->name('posts.delete');
    });
});
