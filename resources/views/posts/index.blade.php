@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Newsline</h3>
                    </div>
                    <div class="panel-body">
                        <div class="panel-heading">
                            <div class="text-right">
                                <a href="{{route('posts.create')}}" class="btn btn-success">Create Post</a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            @if(Session::has('message'))
                                <p class="alert alert-info">{{ Session::get('message') }}</p>
                            @endif
                            @foreach($posts as $post)
                                <div class="row">
                                    <div class="col-lg-4">  {{$post->user->name}}</div>
                                    <div class="col-lg-6">{{$post->created_at->format('Y-m-d H:i:s')}}</div>
                                </div>
                                <div class="text">
                                    <div class="text-info"><a class=""
                                                              href="{{route('posts.show', $post->id)}}">{{ $post->title }}</a>
                                    </div>
                                </div>
                                <span class="text-muted">{{ $post->body }}</span>
                                <br>
                                <div class="col-lg-8">
                                    <a class="btn btn-success"
                                       href="{{route('posts.show', $post->id)}}">Show</a>
                                    <a class="btn btn-warning"
                                       href="{{route('posts.edit', $post->id)}}">Edit</a>
                                    <a class="btn btn-danger" href="{{route('posts.delete', $post->id)}}">Delete</a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                {{$posts->links()}}
            </div>
        </div>
    </div>
@endsection
