@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="panel">
                    <div class="panel-heading">
                        <h3> Edit new Post</h3>
                    </div>
                    <div class="panel-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {{Form::open(['method' => 'post','class' => 'form-horizontal', 'url'=> route('posts.store'),  'enctype' => 'multipart/form-data'])}}
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::text('title', $post->title, ['class' => 'form-control', 'placeholder' => 'Enter title ']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::textarea('body', $post->body, ['class' => 'form-control', 'placeholder' => 'Enter text', 'rows' => 3]) }}
                            </div>
                            <div class="form-group">
                                {{Form::File('photo')}}
                            </div>
                            {{ csrf_field() }}
                            {{Form::submit('Edit',  ['class' => 'btn btn-success'])}}
                        </div>
                        {{Form::open()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
