@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="panel">
                    <div class="panel-heading">
                        <h3>{{$post->title}}</h3>
                    </div>
                    <div class="panel-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="col-md-12">
                            <h3 style="font-weight: bold"> {{$post->title}}</h3>
                            <p>{{$post->body}}</p>
                            <img width="100" height="100"
                                 src="{{ URL::asset('/storage/uploads/'. $post->photo)}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
